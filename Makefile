up:
	SCHEMA=prod docker compose -f docker-compose.yml up
restart:
	SCHEMA=dev docker compose restart flask
down:
	docker compose -f docker-compose.yml down
rmi:
	docker rmi test-flask:latest
rmi_all:
	docker rmi test-flask:latest test-nginx:latest test-mysql:latest
exe:
	docker exec -it flask_sample_2024-flask-1 bash
log:
	docker compose logs -f flask



dup:
	SCHEMA=dev docker compose -f docker-compose_debug.yml up
ddown:
	docker compose -f docker-compose_debug.yml down
drmi:
	docker rmi test-flask-debug:latest
drmi_all:
	docker rmi test-flask-debug:latest test-nginx:latest test-mysql:latest
dexe:
	docker exec -it flask_sample_2024-flask_debug-1 bash

dump:
	docker exec -i catimeta-mysql-1 mysqldump -u root -p catimeta > env/mysql/docker-entrypoint-initdb.d/catimeta_$(shell date +"%Y%m%d").sql

del_volume:
	docker volume rm flask_sample_2024_mysql