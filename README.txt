
本範本根據參考資料網頁調整而成

## 下載本專案
$ git clone https://github.com/amostsai/flask_init.git

## 啟用flask/uwsgi, nginx, mysql服務
$ docker-compose up


## flask除錯模式執行步驟
1. 在vscode按「ctrl + `」打開終端機畫面
2. 執行「SCHEMA=dev docker compose -f docker-compose_debug.yml up」
3. 當出現要按F5按鈕的文字訊息時
4. 按「F5」按鍵啟動除錯模式
5. 在需要中斷的行數左邊點一下（出現紅色中斷點）
6. 打開瀏覽器，連到該功能的網頁觸發該功能
7. 正常的話應該會被中斷停下來
8. 照原本python除錯模式執行、追蹤

p.s. 
第一次在第4步前可能需點左邊的「debug」按鈕
再選「Python: Remote Attach」
再點「綠色三角形」叫可以了
之後直接按F5就可以觸發除錯模式
