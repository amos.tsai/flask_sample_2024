from datetime import timedelta


class Config(object):
    PER_PAGE = 20
    REMEMBER_COOKIE_DURATION = timedelta(days=30)

    CONF_URL = 'https://accounts.google.com/.well-known/openid-configuration'


class ProdConfig(Config):
    DEBUG = False
    TESTING = False

    BASE_URL = 'http://localhost'

    MYSQL_HOST = 'mysql'
    MYSQL_USER = 'test'
    MYSQL_PASSWORD = 'testpassword'
    MYSQL_DB = 'test'
    SQLALCHEMY_DATABASE_URI = f'mysql+pymysql://{MYSQL_USER}:{MYSQL_PASSWORD}@{MYSQL_HOST}/{MYSQL_DB}'
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    SECRET_KEY = 'eidld4kfkvvbof'

    # Flask-Mail.
    MAIL_DEFAULT_SENDER = 'xxxx@gmail.com'  # 要修改！
    MAIL_SERVER = 'smtp.gmail.com'
    MAIL_PORT = 587
    MAIL_USE_TLS = True
    MAIL_USE_SSL = False
    MAIL_USERNAME = 'xxxx@gmail.com'  # 要修改！
    MAIL_PASSWORD = 'xxxxxxxx'  # 要修改！

    GOOGLE_CLIENT_ID = "220655668198-i20cu1ddts2fn8tspajj2cinkem0eotk.apps.googleusercontent.com"
    GOOGLE_CLIENT_SECRET = "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"
    

class DevelopmentConfig(Config):
    DEBUG = True

    BASE_URL = 'http://localhost'

    MYSQL_HOST = 'mysql'
    MYSQL_USER = 'test'
    MYSQL_PASSWORD = 'testpassword'
    MYSQL_DB = 'test'
    SQLALCHEMY_DATABASE_URI = f'mysql+pymysql://{MYSQL_USER}:{MYSQL_PASSWORD}@{MYSQL_HOST}/{MYSQL_DB}'
    SQLALCHEMY_TRACK_MODIFICATIONS = True


    SECRET_KEY = 'eidld4kfkvvbof'

    # Flask-Mail.
    MAIL_DEFAULT_SENDER = 'xxxx@gmail.com'  # 要修改！
    MAIL_SERVER = 'smtp.gmail.com'
    MAIL_PORT = 587
    MAIL_USE_TLS = True
    MAIL_USE_SSL = False
    MAIL_USERNAME = 'xxxx@gmail.com'  # 要修改！
    MAIL_PASSWORD = 'xxxxxxxx'  # 要修改！

    GOOGLE_CLIENT_ID = "220655668198-i20cu1ddts2fn8tspajj2cinkem0eotk.apps.googleusercontent.com"
    GOOGLE_CLIENT_SECRET = "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"