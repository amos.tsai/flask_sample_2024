from flask import Blueprint, render_template, current_app, session
from blueprints.news.model import News_Model

news_bp = Blueprint('news', __name__, template_folder='views', url_prefix="/news")



@news_bp.route('/')
def news_list():
    page = 1
    news = News_Model.query.order_by(News_Model.publish_on.desc()).paginate(page=page, per_page=current_app.config['PER_PAGE'])
    return render_template('index.html', news=enumerate(news.items), user=session['user'])

@news_bp.route('/<int:id>', methods=['GET', 'POST'])
def news_detail(id):
    news = News_Model.query.get_or_404(id)
    return render_template('news_detail.html', news=news, url=current_app.config['BASE_URL'])
