# from datetime import datetime
from extensions import db

class News_Model(db.Model):
    __tablename__ = 'news'

    id = db.Column(db.Integer(), primary_key=True)
    title = db.Column(db.String(255))
    content = db.Column(db.Text())
    publish_on = db.Column(db.DateTime())
    user_id = db.Column(db.Integer(), db.ForeignKey('user.id'))

    def __init__(self, **kwargs):
        super(News_Model, self).__init__(**kwargs)

    def __repr__(self):
        return f"<News_Model '{self.title}'>"