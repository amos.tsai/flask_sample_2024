from datetime import datetime
from extensions import db

class User_Model(db.Model):
    __tablename__ = 'User'

    id = db.Column(db.Integer(), primary_key=True, nullable=False, autoincrement=True)
    name = db.Column(db.String(255), nullable=False)
    lastLoginDateTime = db.Column(db.DateTime(), default=datetime.now())
    sub = db.Column(db.String(30), nullable=False, unique=True)
    email = db.Column(db.String(100), nullable=False, unique=True)
    picture = db.Column(db.String(1000))


    def __init__(self, **kwargs):
        super(User_Model, self).__init__(**kwargs)

    def __repr__(self):
        # 傳回一個符合json格式的字串，方便以後利用key存取。
        return f"""{{
                    "id": {self.id},
                    "name": "{self.name}",
                    "email": "{self.email}",
                }}"""

