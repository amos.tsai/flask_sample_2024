from flask import Blueprint, render_template, current_app
from .model import User_Model

user_bp = Blueprint('user', __name__, template_folder='views', url_prefix="/user")


@user_bp.route('/')
def user_list():
    page = 1
    user = User_Model.query.paginate(page=page, per_page=current_app.config['PER_PAGE'])
    return render_template('index.html', news=user.items)

@user_bp.route('/<int:id>', methods=['GET', 'POST'])
def user_detail(id):
    user = User_Model.query.get_or_404(id)
    return render_template('user_detail.html', user=user, url=current_app.config['BASE_URL'])









# from datetime import datetime
# from flask import Blueprint, render_template, current_app, request, session
# from sqlalchemy import and_

# from extensions import db

# user_bp = Blueprint('user', __name__, template_folder='views', url_prefix="/user")


# class BranchesAllGet(Resource):
#     '''API 70 取得所有分店之店名'''
#     @get_users_lineId_by_ciphertext
#     @swag_from('doc/BranchesAllGet.yml', methods=['GET'])
#     def get(self):
#         branches = Branches_Model.query.all()
#         branchlist = []
#         for i in branches:
#             branchlist.append({'branch_id':i.id, 'branch':i.name, 'branch_address':i.address, 'branch_tel':i.tel})
        
#         return branchlist


# class BranchesOneGet(Resource):
#     '''依據id取得分店完整資料'''
#     @get_users_lineId_by_ciphertext
#     @swag_from('doc/BranchesOneGet.yml', methods=['GET'])
#     def get(self, id):
#         branches = Branches_Model.query.filter_by(id=id).first()
#         return marshal(branches, branches_fields)


# class BranchesOnePost(Resource):
#     '''新增分店'''
#     @get_users_lineId_by_ciphertext
#     @swag_from('doc/BranchesOnePost.yml', methods=['POST'])
#     def post(self):
#         try:
#             request_data = request.get_json()
#             # id = request_data.get('id')

#             branches_data = {
#                 # 'id': id,
#                 'name': request_data.get('name'),
#                 'address': request_data.get('address'),
#                 'tel': request_data.get('tel'),
#                 'beds': request_data.get('beds'),
#                 'manager_name': request_data.get('manager_name'),
#                 'manager_tel': request_data.get('manager_tel'),
#                 'owner_name': request_data.get('owner_name'),
#                 'owner_tel': request_data.get('owner_tel'),
#                 'note': request_data.get('note')
#             }
#             branches = Branches_Model(**branches_data)
#             for key in branches_data:
#                 value = branches_data[key]
#                 setattr(branches, key, value)  # 修改那筆資料。
#                 db.session.add(branches)
#                 db.session.commit()

#             return {
#                 "status": 'successful',
#                 "message": f'分店：{branches.id},新增資料成功'}
#         except Exception as e:
#             return {
#                 "status": 'failed',
#                 "message": f'錯誤訊息：{str(e)}'}, 400




# class BranchesOneDel(Resource):
#     '''依據id刪除單一分店資料'''
#     @get_users_lineId_by_ciphertext
#     @swag_from('doc/BranchesOneDel.yml', methods=['DELETE'])
#     def delete(self, id):
#         try:
#             branches = Branches_Model.query.filter_by(id=id).first()
#             db.session.delete(branches)
#             db.session.commit()
#             return {
#                 "status": 'successful',
#                 "message": f'客戶: {id} 刪除成功'}

#         except Exception as e:
#             return {
#                 "status": 'failed',
#                 "message": f'錯誤訊息：{str(e)}'}
