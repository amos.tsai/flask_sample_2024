from flask_debugtoolbar import DebugToolbarExtension
from flask_sqlalchemy import SQLAlchemy
from authlib.integrations.flask_client import OAuth

# from flask_mail import Mail
# from flask_wtf import CsrfProtect


debug_toolbar = DebugToolbarExtension()
db = SQLAlchemy()
oauth = OAuth()


# mail = Mail()
# csrf = CsrfProtect()