import os
from flask import Flask
from flask import url_for, session, render_template, redirect

from blueprints.news import news_bp
from blueprints.user import user_bp
from extensions import db, debug_toolbar, oauth #, mail, csrf

# debug in container!
from debugger import initialize_flask_server_debugger_if_needed
initialize_flask_server_debugger_if_needed()



def create_app(mode):
    app = Flask(__name__)
    app.config.from_object(mode)

    app.register_blueprint(news_bp)
    app.register_blueprint(user_bp)
    extensions(app)

    return app


def extensions(app):
    debug_toolbar.init_app(app)
    db.init_app(app)
    oauth.init_app(app)

    oauth.register(
    name='google',
    server_metadata_url = app.config['CONF_URL'],
    client_kwargs = {
            'scope': 'openid email profile'
        }
    )
    
    # mail.init_app(app)
    # csrf.init_app(app)



env = os.environ.get('FLASK_ENV', 'prod')
app = create_app('config.%sConfig' % env.capitalize())



@app.route('/')
def homepage():
    user = session.get('user')
    return render_template('/layouts/login.html', user=user)

@app.route('/login')
def login():
    redirect_uri = url_for('auth', _external=True)
    return oauth.google.authorize_redirect(redirect_uri)

@app.route('/auth')
def auth():
    token = oauth.google.authorize_access_token()
    session['user'] = token['userinfo']
    return redirect(url_for('news.news_list'))

@app.route('/logout')
def logout():
    session.pop('user', None)
    return redirect('/')



@app.route('/url_map')  # TODO: 正式上線時要註解掉！
def show_urlmap():
    print(app.url_map)
    return ('', 200)